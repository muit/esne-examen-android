# Práctica Android: 12/02/2018
### Miguel Fernández Arce

Por alguna razón update y render de la escena (World) solo se llaman en el primer frame, y tampoco se cargan los simbolos de depuración.

Esto me ha impedido debugear correctamente y por lo tanto acabar el juego.

### Repositorio: https://gitlab.com/muit/esne-examen-android
