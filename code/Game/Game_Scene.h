// Copyright 2017/2018 - Miguel Fernández Arce

#ifndef GAME_SCENE_HEADER
#define GAME_SCENE_HEADER

    #include "../PCH.h"

    #include <map>
    #include <list>
    #include <memory>

    #include "GameFramework/World.h"

    #include "AssetsPCH.h"

    #include "Player.h"
    #include "Ball.h"


    namespace Game
    {
        using basics::Id;
        using basics::Timer;
        using basics::Canvas;
        using basics::Texture_2D;

        /**
         * Representa el estado del juego cuando el estado de la escena es RUNNING.
         */
        enum Gameplay_State
        {
            UNINITIALIZED,
            WAITING_TO_START,
            MATCH_PLAYING,
            AFTER_DEATH,
            MATCH_FINISHED
        };

        class Game_Scene : public World
        {

        public:

            APlayer* playerA;
            APlayer* playerB;
            ABall*   ball;
            list<ASprite*> walls;

        private:

            Gameplay_State gameplay;                            ///< Estado del juego cuando la escena está RUNNING.

            Timer timer;                               ///< Cronómetro usado para medir intervalos de tiempo


        public:

            /**
             * Aquí se inicializan los atributos que deben restablecerse cada vez que se inicia la escena.
             * @return
             */
            virtual bool Initialize () override;


            // HANDLERS
            inline Gameplay_State GetGameState() const { return gameplay;  }
            void SetGameState(Gameplay_State state);


        protected:

            /**
             * En este método se crean los sprites cuando termina la carga de texturas.
             */
            virtual void GameStart() override;

            virtual void MatchStart();

            /**
             * Se llama cada vez que se debe reiniciar el juego. En concreto la primera vez y cada
             * vez que un jugador pierde.
             */
            virtual void MatchFinish();

            virtual void OnTouchEvent(TouchState state, Vector2D position) override;
        };

    }

#endif
