// Copyright 2017/2018 - Miguel Fernández Arce

#include "Player.h"
#include <basics/Log>

#include "Game_Scene.h"


namespace Game
{
    const AssetPtr<Texture> APlayer::asset { ID(player-bar) };

    void APlayer::Reset() {
        const World* world = GetWorld();
        if(!world)
            return;

        if(id == PlayerId::PLAYER_A)
        {
            SetPosition({ 10.f, float(world->canvas_height)/2 });
        }
        else
        {
            SetPosition({ float(world->canvas_width) - 10.f, float(world->canvas_height)/2 });
        }

        targetY = GetPosition().y;
    }

    void APlayer::Update (float time)
    {
        const Game_Scene* scene = static_cast<const Game_Scene*>(GetWorld());
        if(!scene)
            return;

        const Vector2D position = GetPosition();
        const bool bMove = abs(targetY - position.y) > 10;


        if(bMove)
        {
            // Go Down
            if(GetPosition().y > targetY) {
                movement.velocity = {0, -speed};
            }
            // Go Up
            else if(GetPosition().y < targetY)
            {
                movement.velocity = {0, speed};
            }

            for(const auto* wall : scene->walls)
            {
                if(wall && CollidesWith(*wall))
                {
                    if(wall->GetPosition().y < position.y + 30 && movement.velocity.y < 0)
                    {
                        movement.velocity = Vector2D::Zero;
                        break;
                    }
                    else
                    if(wall->GetPosition().y > position.y - 30 && movement.velocity.y > 0)
                    {
                        movement.velocity = Vector2D::Zero;
                        break;
                    }
                }
            }
        }
        else
        {
            movement.velocity = Vector2D::Zero;
        }

        // Collisions
        ASprite::Update(time);
    }

    void APlayer::OnTouchEvent(TouchState state, Vector2D position)
    {
        const Game_Scene* scene = static_cast<const Game_Scene*>(GetWorld());
        if(!scene)
            return;

        // Does touch event correspond to this player?
        if((id == PlayerId::PLAYER_A && position.x < scene->canvas_width/2) ||
           (id == PlayerId::PLAYER_B && position.x > scene->canvas_width/2))
        {
            if(state == Press)
            {
                targetY = position.y;
            }
            else if(state == Release)
            {
            }
        }
    }
}