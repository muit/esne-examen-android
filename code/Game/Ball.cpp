// Copyright 2017/2018 - Miguel Fernández Arce

#include "Ball.h"
#include "Game_Scene.h"

#include <basics/Log>

namespace Game
{
    const AssetPtr<Texture> ABall::asset { ID(circle) };

    void ABall::Update(float deltaTime)
    {
        Game_Scene* scene = static_cast<Game_Scene*>(GetWorld());

        if(!scene || scene->GetGameState() != MATCH_PLAYING)
            return;

        if(GetPosition().x < 0 || GetPosition().x > scene->canvas_width)
        {
            scene->SetGameState(MATCH_FINISHED);
            return;
        }

        for(const auto* wall : scene->walls)
        {
            if(wall && CollidesWith(*wall)) {
                movement.velocity.y = -movement.velocity.y;
            }
        }

        if(scene->playerA && CollidesWith(*scene->playerA) && movement.velocity.x < 0)
        {
            movement.velocity.x = -movement.velocity.x;
        }

        if(scene->playerB && CollidesWith(*scene->playerB) && movement.velocity.x > 0)
        {
            movement.velocity.x = -movement.velocity.x;
        }

        Actor::Update(deltaTime);
    }
}
