// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include "../PCH.h"

#include <basics/Canvas>
#include <basics/Texture_2D>
#include <basics/Vector>
#include <basics/Timer>

#include "GameFramework/World.h"
#include "Actors/Sprite.h"
#include "Components/SimpleMovement.h"

using namespace basics;
using namespace Engine;


namespace Game
{
    //Forward declarations
    class Game_Scene;


    enum class PlayerId : uint8_t  {
        PLAYER_A,
        PLAYER_B
    };


    class APlayer : public ASprite
    {
        static const AssetPtr<Texture> asset;

    public:
        PlayerId id;
        CSimpleMovement& movement;

        float speed = 175;

    private:
        float targetY;

    public:

        APlayer(PlayerId id)
            : ASprite(asset.Get())
            , id{ id }
            , movement{ *CreateComponent<CSimpleMovement>() }
        {
            EnableCollision();
        }

        virtual void Reset(); 

        virtual void Update(float time) override;

        virtual void OnTouchEvent(TouchState state, Vector2D position);
    };
}
