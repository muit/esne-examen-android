// Copyright 2017/2018 - Miguel Fernández Arce

#include "Game_Scene.h"

#include <cstdlib>
#include <basics/Canvas>
#include <basics/Director>

using namespace basics;
using namespace std;

namespace Game
{
    // ---------------------------------------------------------------------------------------------
    // ID y ruta de las texturas que se deben cargar para esta escena. La textura con el mensaje de
    // carga está la primera para poder dibujarla cuanto antes:

    // ---------------------------------------------------------------------------------------------
    // Definiciones de los atributos estáticos de la clase:


    // ---------------------------------------------------------------------------------------------
    // Algunos atributos se inicializan en este método en lugar de hacerlo en el constructor porque
    // este método puede ser llamado más veces para restablecer el estado de la escena y el constructor
    // solo se invoca una vez.

    bool Game_Scene::Initialize ()
    {
        SetGameState(UNINITIALIZED);

        assets = {
            { ID(hbar),       "game-scene/horizontal-bar.png" },
            { ID(player-bar), "game-scene/player-bar.png"     },
            { ID(circle),     "game-scene/circle.png"         },
            { ID(box),        "game-scene/box.png"            }
        };

        return true;
    }


    // ---------------------------------------------------------------------------------------------

    void Game_Scene::GameStart()
    {
        // NOTE: Scene deals with memory collection though CreateActor and DestroyActor
        //       We don't need to free it.

        // Create walls
        {
            ASprite* top_wall = CreateActor<ASprite>(assets[ID(hbar)].Get());
            top_wall->EnableCollision();
            top_wall->SetAnchor(TOP | LEFT);
            top_wall->SetPosition ({ 0, float(canvas_height) });
            walls.push_back(top_wall);

            ASprite* bottom_wall = CreateActor<ASprite>(assets[ID(hbar)].Get());
            bottom_wall->EnableCollision();
            bottom_wall->SetAnchor(BOTTOM | LEFT);
            bottom_wall->SetPosition ({ 0, 0 });
            walls.push_back(bottom_wall);
        }

        playerA = CreateActor<APlayer>(PlayerId::PLAYER_A);
        playerA->Reset();
        playerB = CreateActor<APlayer>(PlayerId::PLAYER_B);
        playerB->Reset();

        ball = CreateActor<ABall>();
        ball->SetPosition({ float(canvas_width)/2, float(canvas_height)/2 });

        SetGameState(WAITING_TO_START);
    }

    void Game_Scene::MatchStart() {
        bool toLeft = rand()%2 >= 1;
        ball->movement.velocity = Vector2D::FromAngle((toLeft? 15 : 195) + rand()%45, 350);
    }

    void Game_Scene::MatchFinish()
    {
        playerA->Reset();
        playerB->Reset();

        if(ball) {
            ball->SetPosition({ float(canvas_width)/2, float(canvas_height)/2 });
        }

        SetGameState(WAITING_TO_START);
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::OnTouchEvent(TouchState state, Vector2D position)
    {
        if(gameplay == WAITING_TO_START)
        {
            SetGameState(MATCH_PLAYING);
        }
        else
        {
            playerA->OnTouchEvent(state, position);
            playerB->OnTouchEvent(state, position);
        }
    }

    void Game_Scene::SetGameState(Gameplay_State state)
    {
        if(gameplay != state)
        {
            //If state changes
            gameplay = state;

            if(gameplay == MATCH_PLAYING)
            {
                MatchStart();
            }
            else if(gameplay == MATCH_FINISHED)
            {
                MatchFinish();
            }
        }
    }
}
