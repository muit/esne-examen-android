// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include "../PCH.h"

#include "Actors/Sprite.h"

#include "AssetPtr.h"
#include "Assets/Texture.h"
#include "Components/SimpleMovement.h"


using namespace Engine;


namespace Game
{
    class ABall : public ASprite
    {
        static const AssetPtr<Texture> asset;

    public:

        CSimpleMovement& movement;


        ABall()
            : ASprite(asset.Get())
            , movement{*CreateComponent<CSimpleMovement>()}
        {
            EnableCollision();
        }

        virtual void Update(float deltaTime) override;
    };
}
