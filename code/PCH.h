
#pragma once

#include <cstdlib>

#include <map>
#include <list>
#include <memory>
#include <basics/Id>
#include <basics/Texture_2D>

#include "Engine/Actors/Sprite.h"

using namespace std;
using namespace basics;
using namespace Engine;

using basics::Id;
using basics::Canvas;
using basics::Texture_2D;

typedef shared_ptr<ASprite> SpritePtr;
typedef weak_ptr<ASprite>   SpriteWeakPtr;
typedef list<SpritePtr>        SpriteList;
typedef list<SpriteWeakPtr>    SpriteWeakList;
typedef basics::Graphics_Context::Accessor Context;
