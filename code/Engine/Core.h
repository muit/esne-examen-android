// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include <cstdlib>

#include <map>
#include <list>
#include <memory>
#include <basics/Id>
#include <basics/Canvas>
#include <basics/Texture_2D>
#include "Math/Vector2D.h"

using namespace std;
using namespace basics;

using basics::Id;
using basics::Texture_2D;

typedef basics::Graphics_Context::Accessor Context;
