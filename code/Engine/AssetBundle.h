// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include <basics/Director>

#include "AssetPtr.h"

using namespace basics;


namespace Engine
{
    template<class T>
    class AssetBundle
    {
    public:

        AssetBundle() : assets() {}
        AssetBundle(initializer_list<AssetPtr<T>>&& list) : assets{ list } {}
        AssetBundle(vector<AssetPtr<T>> inAssets) : assets(move(inAssets)) {}

    protected:
        
        vector<AssetPtr<T>> assets;

    public:
        
        const AssetPtr<T> Find(const Id id) const
        {
            auto resultIt = find_if(assets.begin(), assets.end(), [&id] (const AssetPtr<T>& asset)
            { 
                return asset.GetId() == id;
            });

            if(resultIt != assets.end())
            {
                return *resultIt;
            }

            return {};
        }

        inline const AssetPtr<T> operator[](const Id id) const
        {
            return Find(id);
        }

        inline const vector<AssetPtr<T>> GetUnloadedAssets() {
            vector<AssetPtr<T>> unloadedAssets;

            for (auto it = assets.begin(), end = assets.end(); it != end; ++it)
                if (!(*it).IsNull() && !(*it).IsValid())
                    unloadedAssets.push_back(*it);
            return unloadedAssets;
        }


        inline const vector<AssetPtr<T>>& GetAssets() { return assets; }

    };
}
