// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include "Core.h"

#include "AssetManager.h"
#include "AssetInfo.h"
#include "Assets/AssetData.h"

using namespace std;
using namespace basics;


namespace Engine
{
    template<class T>
    class AssetPtr : public AssetInfo
    {
        static_assert(is_base_of<AssetData, T>::value, "Template must inherit from AssetData");

    private:

        mutable shared_ptr<T> cachedAsset;

    public:
        AssetPtr() : AssetInfo(), cachedAsset(nullptr) {}
        AssetPtr(Id inId)
            : AssetInfo(inId)
            , cachedAsset(nullptr)
        {}
        AssetPtr(Id inId, string inPath)
            : AssetInfo(inId, inPath)
            , cachedAsset(nullptr)
        {}

        /**
         * @returns true if this asset is loaded 
         */
        const bool IsValid() const {
            return Get() != nullptr;
        }

        /**
         * Tries to load this asset if it's not already
         * @returns the loaded asset
         */
        const shared_ptr<T> Load(Director &director) const {
            if(IsNull())
                return nullptr;

            if(!IsValid())
                cachedAsset = AssetManager::Get().TryLoad<T>(director, *this);

            return cachedAsset;
        }

        /**
         * @returns the asset if it's valid and loaded
         */
        shared_ptr<T> Get() const {
            if(IsNull())
                return nullptr;

            if(!cachedAsset)
                cachedAsset = AssetManager::Get().GetAssetById<T>(id);

            return cachedAsset;
        }
    };
}
