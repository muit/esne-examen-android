// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include "GameFramework/SceneComponent.h"

using namespace std;


namespace Engine
{
    class CSimpleMovement : public SceneComponent
    {
    public:

        Vector2D velocity;


        CSimpleMovement() : SceneComponent() {}

        virtual void Update (float deltaTime) override;
    };
}
