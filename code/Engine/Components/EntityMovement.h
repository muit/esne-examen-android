// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include "SimpleMovement.h"


namespace Engine
{
    class CEntityMovement : public CSimpleMovement
    {
    public:
        static const float gravity;

        float gravityMultiplier = 1;
        float acceleration = 10;
        float maxSpeed = 160;
        float jumpImpulse = 160;


    public:

        virtual void Update(float deltaTime) override;

        float GetGravity() const { return gravity * gravityMultiplier; }
    };
}
