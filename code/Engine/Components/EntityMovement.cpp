// Copyright 2017/2018 - Miguel Fernández Arce

#include "EntityMovement.h"

using namespace basics;


namespace Engine
{
    const float CEntityMovement::gravity = 9.8f;

    void CEntityMovement::Update(float deltaTime)
    {
        CSimpleMovement::Update(deltaTime);
    }
}
