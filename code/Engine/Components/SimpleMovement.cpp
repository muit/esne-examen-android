// Copyright 2017/2018 - Miguel Fernández Arce

#include "SimpleMovement.h"

using namespace basics;


namespace Engine
{
    void CSimpleMovement::Update(float deltaTime)
    {
        if (IsActive())
        {
            const Vector2D displacement = velocity * deltaTime;
            GetOwner()->SetPosition(GetOwner()->GetPosition() + displacement);
        }
    }
}
