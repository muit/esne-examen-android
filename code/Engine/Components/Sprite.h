// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include <memory>
#include <basics/Canvas>
#include <basics/Texture_2D>
#include <basics/Vector>
#include <basics/Log>


#include "Assets/Texture.h"
#include "GameFramework/SceneComponent.h"

using namespace std;


namespace Engine
{
    using basics::Canvas;
    using basics::Size2f;
    using basics::Point2f;
    using basics::Vector2f;
    using basics::Texture_2D;

    class CSprite : public SceneComponent
    {
    protected:

        Texture_2D * texture;                   ///< Textura en la que está la imagen del sprite.
        int          anchor;                    ///< Indica qué punto de la textura se colocará en 'position' (x,y).

        Vector2D size;                      ///< Tamaño del sprite (normalmente en coordenadas virtuales).

    public:

        /**
         * Inicializa una nueva instancia de Sprite.
         * @param texture Puntero a la textura en la que está su imagen. No debe ser nullptr.
         */
        CSprite();
        CSprite(Texture_2D * texture);
        CSprite(const shared_ptr<Texture>& texture)
            : CSprite(texture? texture->GetTexture() : nullptr)
        {}

        // Getters (con nombres autoexplicativos):

        const Vector2D& GetSize  () const { return  size;   }
        const float&    GetWidth () const { return  size.x; }
        const float&    GetHeight() const { return  size.y; }

        float GetLeftX () const
        {
            const Vector2D position = GetPosition();
            return
                (anchor & 0x3) == basics::LEFT  ? position.x :
                (anchor & 0x3) == basics::RIGHT ? position.x - size.x :
                    position.x - size.x * .5f;
        }

        float GetRightX () const
        {
            return GetLeftX () + size.x;
        }

        float GetBottomY () const
        {
            const Vector2D position = GetPosition();
            return
                (anchor & 0xC) == basics::BOTTOM ? position.y :
                (anchor & 0xC) == basics::TOP    ? position.y - size.y :
                    position.y - size.y * .5f;
        }

        float GetTopY () const
        {
            return GetBottomY() + size.y;
        }

        // Setters (con nombres autoexplicativos):

        void SetAnchor(int newAnchor)
        {
            anchor = newAnchor;
        }


        /**
         * Comprueba si el área envolvente rectangular de este sprite se solapa con la de otro.
         * @param other Referencia al otro sprite.
         * @return true si las áreas se solapan o false en caso contrario.
         */
        virtual bool CollidesWith(const SceneComponent & other) override;

        /**
         * Comprueba si un punto está dentro del sprite.
         * Se puede usar para, por ejemplo, comprobar si el punto donde el usuario ha tocado la
         * pantalla está dentro del sprite.
         * @param point Referencia al punto que se comprobará.
         * @return true si el punto está dentro o false si está fuera.
         */
        virtual bool CollidesWith(const Vector2D& point) override;

        virtual void Render (Canvas & canvas) override
        {
            if (IsActive())
            {
                if(!texture) {
                    basics::log.w("Sprite texture not found!");
                    return;
                }

                canvas.fill_rectangle(GetPosition().ToPoint(), (size * GetScale()).ToSize(), texture, anchor);
            }
        }

    };
}
