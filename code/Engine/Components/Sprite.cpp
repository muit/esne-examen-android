// Copyright 2017/2018 - Miguel Fernández Arce

#include "Sprite.h"

using namespace basics;


namespace Engine
{

    CSprite::CSprite()
        : SceneComponent()
        , texture(nullptr)
        , anchor { basics::CENTER }
        , size   { 0, 0 }
    {
        EnableCollision();
    }

    CSprite::CSprite(Texture_2D* texture)
        : SceneComponent()
        , texture (texture)
    {
        EnableCollision();

        anchor   = basics::CENTER;
        if(texture) {
            size = { texture->get_width (), texture->get_height () };
        }
    }

    bool CSprite::CollidesWith(const SceneComponent& other)
    {
        if(!IsCollisionEnabled())
            return false;

        const CSprite* otherSprite = dynamic_cast<const CSprite*>(&other);

        if(!otherSprite || !otherSprite->IsCollisionEnabled())
            return false;

        // Se determinan las coordenadas de la esquina inferior izquierda y de la superior derecha
        // de este sprite:

        const float this_left    = GetLeftX   ();
        const float this_bottom  = GetBottomY ();
        const float this_right   = this_left   + size.x;
        const float this_top     = this_bottom + size.y;

        // Se determinan las coordenadas de la esquina inferior izquierda y de la superior derecha
        // del otro sprite:

        const float other_left   = otherSprite->GetLeftX();
        const float other_bottom = otherSprite->GetBottomY();
        const float other_right  = other_left   + otherSprite->size.x;
        const float other_top    = other_bottom + otherSprite->size.y;

        // Se determina si los rectángulos envolventes de ambos sprites se solapan:

        return !(other_left >= this_right || other_right <= this_left || other_bottom >= this_top || other_top <= this_bottom);
    }

    bool CSprite::CollidesWith(const Vector2D& point)
    {
        const float this_left = GetLeftX();
        if (point.x > this_left)
        {
            const float this_bottom = GetBottomY();
            if (point.y > this_bottom)
            {
                const float this_right = this_left + size.x;
                if (point.x < this_right)
                {
                    const float this_top = this_bottom + size.y;
                    if (point.y < this_top)
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
