// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include "../GameFramework/Actor.h"
#include "../Components/EntityMovement.h"

namespace Engine
{
    class AEntity : public Actor
    {
    public:
    
        CEntityMovement* movement;


        AEntity()
            : Actor()
            , movement { CreateComponent<CEntityMovement>() }
        {}
    };
}
