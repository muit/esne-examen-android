// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include "GameFramework/Actor.h"
#include "Components/Sprite.h"


namespace Engine
{
    class ASprite : public Actor
    {

    public:

        CSprite& sprite;

        ASprite()
            : Actor()
            , sprite{ *CreateComponent<CSprite>() }
        {}
        ASprite(shared_ptr<Texture> texture)
            : Actor()
            , sprite{ *CreateComponent<CSprite>(texture) }
        {}

        inline void SetAnchor(int newAnchor) const
        {
            sprite.SetAnchor(newAnchor);
        }
    };
}
