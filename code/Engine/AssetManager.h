// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include "Core.h"

#include "Assets/AssetData.h"
#include "AssetInfo.h"

using namespace std;
using namespace basics;


namespace Engine
{
    class AssetManager
    {
    private:

        vector<AssetInfo> assetInfos;

        map<Id, shared_ptr<AssetData>> loadedAssets;


        void Internal_StaticLoad(const AssetInfo& asset);
        void Internal_StaticLoad(const vector<AssetInfo>& assets);

    public:

        /*void Initialize() {}

        void Shutdown() {}*/

        template<class T>
        shared_ptr<T> TryLoad(Director& director, const AssetInfo& info) {
            static_assert(is_base_of<AssetData, T>::value, "Template must inherit from AssetData");

            if(info.IsNull() || info.GetPath().empty())
                return nullptr;

            shared_ptr<T> newAsset = make_shared<T>(director, info);
            if(newAsset->Construct(director, info))
            {
                // Loading succeeded, registry the asset
                loadedAssets[info.GetId()] = newAsset;
                return newAsset;
            }
            return nullptr;
        }

        inline const shared_ptr<AssetData>& GetAssetById(Id id) {
            return loadedAssets[id];
        }
        template<class T>
        inline shared_ptr<T> GetAssetById(Id id) {
            static_assert(is_base_of<AssetData, T>::value, "Template must inherit from AssetData");
            return static_pointer_cast<T>(loadedAssets[id]);
        }

    private:

        static AssetManager instance;

    public:

        //TODO: Static Loading
        /*static void StaticLoad(const AssetInfo& asset) {
            instance.Internal_StaticLoad(asset);
        }

        static void StaticLoads(const vector<AssetInfo>& assets) {
            instance.Internal_StaticLoad(assets);
        }*/

        /*static void Registry() {
            instance.Initialize();
        }
        static void Unregistry() {
            instance.Shutdown();
        }*/

        static AssetManager& Get() { return instance; }
    };
}
