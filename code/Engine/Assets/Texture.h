// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include <basics/Texture_2D>
#include "AssetData.h"


namespace Engine
{
    class Texture : public AssetData
    {
    protected:
        shared_ptr<Texture_2D> data;

    public:
        //Called when loading the asset
        Texture(Director& director, const AssetInfo& info)
            : AssetData(director, info)
            , data{nullptr}
        {}

        virtual bool Construct(Director& director, const AssetInfo& info) override;

        Texture_2D* GetTexture() const {
            return data? data.get() : nullptr;
        }
    };
}
