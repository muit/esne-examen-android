// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include <basics/Director>

using namespace std;
using namespace basics;


namespace Engine
{
    class AssetInfo;

    class AssetData
    {
    protected:

        AssetData() {}

    public:

        //Called when loading the asset
        AssetData(Director& director, const AssetInfo& info) {}

        virtual bool Construct(Director& director, const AssetInfo& info)
        { return true; }
    };
}
