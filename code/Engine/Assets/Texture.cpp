// Copyright 2017/2018 - Miguel Fernández Arce

#include <basics/Director>

#include "../AssetInfo.h"

#include "Texture.h"


namespace Engine {
    bool Texture::Construct(Director& director, const AssetInfo &info)
    {
        Graphics_Context::Accessor context = director.lock_graphics_context ();

        if (context)
        {
            data = Texture_2D::create (info.GetId(), context, info.GetPath());

            if(data) {
                context->add (data);
                return true;
            }
        }
        return false;
    }
}
