// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include "Core.h"
#include "Actor.h"
#include "Component.h"


namespace Engine
{
    class SceneComponent : public Component
    {
    public:

        SceneComponent()
            : Component()
            , relativePosition { Vector2D::Zero }
            , relativeScale    { Vector2D::One  }
            , collisionEnabled { false }
        {}


    private:
        // Transform
        Vector2D relativePosition;
        Vector2D relativeScale;

    protected:

        // Collision
        bool collisionEnabled;


    public:

        virtual void Start() override {
            Component::Start();
        }

        virtual void Update(float deltaTime) override {
            Component::Update(deltaTime);
        }

        virtual void Render(Canvas& canvas) override {
            Component::Render(canvas);
        }


        /**
         * Check if this SceneComponent collides with another
         * @param other The other component
         * @return true if there is a collision
         */
        virtual bool CollidesWith(const SceneComponent & other) { return false; }

        /**
         * Check if this SceneComponent collides with a point in space
         * @param point in space
         * @return true if there is a collision
         */
        virtual bool CollidesWith(const Vector2D& point) { return false; }

        void EnableCollision()  { collisionEnabled = true; }
        void DisableCollision() { collisionEnabled = false; }
        inline bool IsCollisionEnabled() const { return collisionEnabled; }


        inline const Vector2D& GetRelativePosition() const {
            return relativePosition;
        }

        inline void SetRelativePosition(const Vector2D& newPosition) {
            relativePosition = newPosition;
        }

        inline Vector2D GetPosition() const {
            return GetOwner()->GetPosition() + GetRelativePosition();
        }

        inline void SetPosition(const Vector2D& newPosition) {
            relativePosition = newPosition - GetOwner()->GetPosition();
        }


        inline Vector2D GetRelativeScale() const {
            return relativeScale;
        }

        inline void SetRelativeScale(const Vector2D& newScale) {
            relativeScale = newScale;
        }

        inline Vector2D GetScale() const {
            return GetOwner()->GetScale() * GetRelativeScale();
        }

        inline void SetScale(const Vector2D& newScale) {
            relativeScale = newScale / GetOwner()->GetScale();
        }
    };
}
