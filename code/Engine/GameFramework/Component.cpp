// Copyright 2017/2018 - Miguel Fernández Arce

#include "Component.h"
#include "Actor.h"

namespace Engine {
    void Component::SetActive(bool state)
    {
        if(active != state)
        {
            active = state;

            if(IsInitialized())
            {
                if(active)
                {
                    Activated();
                }
                else
                {
                    Deactivated();
                }
            }
        }
    }
}
