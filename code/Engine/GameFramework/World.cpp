// Copyright 2017/2018 - Miguel Fernández Arce

#include "World.h"

#include <basics/Director>

namespace Engine {
    const AssetPtr<Texture> World::loadingTexture { ID(loading), "game-scene/loading.png" };

    World::World() : basics::Scene()
    {
        //Default resolution
        canvas_width  = 1280;
        canvas_height =  720;
        
        //Random seed
        srand (unsigned(time(nullptr)));

        assets = {};
    }


    void World::Update(float deltaTime)
    {
        for (auto& actor : actors)
        {
            actor->Update(deltaTime);
        }
    }

    void World::Render(Canvas& canvas)
    {
        for (auto& actor : actors)
        {
            actor->Render(canvas);
        }
    }

    // ---------------------------------------------------------------------------------------------
    // En este método solo se carga un asset por fotograma para poder pausar la carga si el
    // juego pasa a segundo plano inesperadamente. Otro aspecto interesante es que la carga no
    // comienza hasta que la escena se inicia para así tener la posibilidad de mostrar al usuario
    // que la carga está en curso en lugar de tener una pantalla en negro que no responde durante
    // un tiempo.
    void World::LoadingUpdate(float deltaTime)
    {
        loadingTexture.Load(director);

        const vector<AssetPtr<Texture>> unloadedAssets = assets.GetUnloadedAssets();

        if(unloadedAssets.size() > 0)
        {
            const AssetPtr<Texture>& assetPtr = unloadedAssets[0];
            if(!assetPtr.Load(director))
            {
                // Mark error if should load (but couldn't)
                state = ERROR;
            }
            //Only load 1 texture every frame
            return;
        }
        else if (timer.get_elapsed_seconds () > 1.f)
        {
            StartGame();
        }
    }

    void World::LoadingRender(Canvas& canvas)
    {
        Texture_2D* loading_texture = loadingTexture.Get()->GetTexture();
        if (loading_texture)
        {
            canvas.fill_rectangle
            (
                { canvas_width * .5f, canvas_height * .5f },
                { loading_texture->get_width (), loading_texture->get_height () },
                  loading_texture
            );
        }
    }


    bool World::PreInitialize()
    {
        if(state == NONE) {
            state = LOADING;
        }

        return Initialize();
    }

    void World::suspend() { suspended = true; }
    void World::resume() { suspended = false; }

    void World::handle (basics::Event & event)
    {
        if (state == RUNNING || state == FINISHED)
        {
            switch (event.id)
            {
                case ID(touch-started):
                    touchPosition = { *event[ID(x)].as< var::Float > (), *event[ID(y)].as< var::Float > () };
                    touching = Press;
                    OnTouchEvent(Press, touchPosition);
                    break;
                case ID(touch-moved):
                    touchPosition = { *event[ID(x)].as< var::Float > (), *event[ID(y)].as< var::Float > () };
                    touching = Move;
                    OnTouchEvent(Move, touchPosition);
                    break;

                case ID(touch-ended):
                {
                    touching = Release;
                    OnTouchEvent(Release, touchPosition);
                    break;
                }
            }
        }
    }
    
    void World::update (float deltaTime)
    {
        if (!suspended) switch (state)
        {
            case LOADING:
                LoadingUpdate(deltaTime);
                break;
            case RUNNING:
                Update(deltaTime);
                break;
            default:
                break;
        }

        //Reset touch after update
        touching = None;
    }

    void World::render (Context & context)
    {
        if (!suspended)
        {
            // El canvas se puede haber creado previamente, en cuyo caso solo hay que pedirlo:

            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            // Si no se ha creado previamente, hay que crearlo una vez:
            if (!canvas)
            {
                 canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            // Si el canvas se ha podido obtener o crear, se puede dibujar con él:
            if (canvas)
            {
                canvas->clear ();

                switch (state)
                {
                    case LOADING:
                        LoadingRender(*canvas);
                        break;
                    case RUNNING:
                        Render(*canvas);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    bool World::initialize() {
        return PreInitialize();
    }
}
