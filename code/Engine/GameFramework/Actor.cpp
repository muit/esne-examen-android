// Copyright 2017/2018 - Miguel Fernández Arce

#include "Actor.h"
#include "SceneComponent.h"


namespace Engine {
    
    void Actor::Start()
    {
        for(const unique_ptr<Component>& component : components)
        {
            component->Start();
        }
    }

    void Actor::Update(float deltaTime)
    {
        for(const unique_ptr<Component>& component : components)
        {
            component->Update(deltaTime);
        }
    }

    void Actor::Render(Canvas& canvas)
    {
        for(const unique_ptr<Component>& component : components)
        {
            component->Render(canvas);
        }
    }


    void Actor::DestroyComponent(const Component& comp)
    {
        components.erase(
            remove_if(components.begin(), components.end(), [comp](const unique_ptr<Component>& x) {
                return x.get() == &comp;
            }), components.end()
        );
    }


    bool Actor::CollidesWith(const Actor& other)
    {
        if(this == &other || !IsCollisionEnabled() || !other.IsCollisionEnabled())
            return false;

        for(const unique_ptr<Component>& component : components)
        {
            SceneComponent* comp = dynamic_cast<SceneComponent*>(component.get());
            if(comp && comp->IsCollisionEnabled())
            {
                for(const unique_ptr<Component>& component : other.components)
                {
                    const SceneComponent* otherComp = dynamic_cast<const SceneComponent*>(component.get());
                    if(otherComp && comp->CollidesWith(*otherComp))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    bool Actor::CollidesWith(const Vector2D& point)
    {
        if(!IsCollisionEnabled())
            return false;

        for(const unique_ptr<Component>& component : components)
        {
            SceneComponent* comp = dynamic_cast<SceneComponent*>(component.get());
            if(comp && comp->CollidesWith(point))
            {
                return true;
            }
        }
        return false;
    }
}
