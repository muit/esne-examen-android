// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include "../PCH.h"

#include <map>
#include <list>
#include <memory>

#include <basics/Canvas>
#include <basics/Id>
#include <basics/Scene>
#include <basics/Timer>

#include "../Engine/AssetsPCH.h"


namespace Engine
{
    using namespace basics;
    using basics::Id;
    using basics::Timer;
    using basics::Canvas;
    using basics::Texture_2D;

    enum TouchState
    {
        None,
        Press,
        Move,
        Release
    };

    class World : public basics::Scene, public std::enable_shared_from_this<World>
    {
        /**
         * Representa el estado de la escena en su conjunto.
         */
        enum State
        {
            NONE,
            LOADING,
            RUNNING,
            FINISHED,
            ERROR
        };


        static const AssetPtr<Texture> loadingTexture;

    public:

        unsigned canvas_width;                        ///< Ancho de la resolución virtual usada para dibujar.
        unsigned canvas_height;                       ///< Alto  de la resolución virtual usada para dibujar.

        TouchState touching    = TouchState::None;
        Vector2D touchPosition = Vector2D::Zero;

    protected:

        // Assets to be loaded at start
        AssetBundle<Texture> assets;

        // Actors owned by this scene
        list<unique_ptr<Actor>> actors;

    private:

        State state = NONE;                               ///< Estado de la escena.
        bool  suspended = true;                           ///< true cuando la escena está en segundo plano y viceversa.

        Timer timer;                               ///< Cronómetro usado para medir intervalos de tiempo


    public:

        World();

        /** BEGIN WORLD INTERFACE */

        void StartGame(){
            if(state != RUNNING)
            {
                state = RUNNING;
                GameStart();
            }
        }

        void FinishGame(){
            if(state == RUNNING)
            {
                state = FINISHED;
                GameFinish();
            }
        }

    private:

        bool PreInitialize();

    protected:

        virtual bool Initialize() { return true; }
        virtual void GameStart() {}

        virtual void GameFinish() {}

        virtual void Update(float deltaTime);
        virtual void Render(Canvas& canvas);

        virtual void LoadingUpdate(float deltaTime);
        virtual void LoadingRender(Canvas& canvas);

        virtual void OnTouchEvent(TouchState state, Vector2D position) {}

    public:

        template< class T, class... Args >
        T* CreateActor(Args&&... args){
            static_assert(is_base_of<Actor, T>::value, "Template must inherit from Actor");

            unique_ptr<Actor> newActor { new T(args...) };
            T* rawPtr = static_cast<T*>(newActor.get());

            newActor->Initialize(shared_from_this());
            actors.push_back(move(newActor));
            return rawPtr;
        }

        /** END WORLD INTERFACE */

    public:

        /** BEGIN SCENE INTERFACE */


        virtual bool initialize() override;

        /**
         * Este método lo invoca Director automáticamente cuando el juego pasa a segundo plano.
         */
        void suspend () override;

            /**
             * Este método lo invoca Director automáticamente cuando el juego pasa a primer plano.
             */
        void resume () override;

        /**
         * Este método se invoca automáticamente una vez por fotograma cuando se acumulan
         * eventos dirigidos a la escena.
         */
        void handle (basics::Event & event) override;

        /**
         * Este método se invoca automáticamente una vez por fotograma para que la escena
         * actualize su estado.
         */
        void update (float time) override;

        /**
         * Este método se invoca automáticamente una vez por fotograma para que la escena
         * dibuje su contenido.
         */
        void render (Context & context) override;

        /**
         * Este método lo llama Director para conocer la resolución virtual con la que está
         * trabajando la escena.
         * @return Tamaño en coordenadas virtuales que está usando la escena.
         */
        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        /** END SCENE INTERFACE */
    };
}