// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include "Core.h"

namespace Engine
{
    class Actor;

    class Component
    {
    public:
        Component()
            : owner(nullptr)
            , active(true)
        {}

        virtual ~Component() {
            SetActive(false);
        };


    private:
        bool active;
        Actor* owner;


    public:

        virtual void Initialize(Actor* inOwner)
        {
            // Should never call Initialize after first initialization!
            assert(!IsInitialized());

            owner = inOwner;

            if(IsActive())
            {
                Activated();
            }
        }

        virtual void Activated() {}

        virtual void Start() {}

        virtual void Update(float deltaTime) {}

        virtual void Render(Canvas& canvas) {}

        virtual void Deactivated() {}


        inline Actor* GetOwner()    const { return owner; }
        inline bool IsInitialized() const { return owner; }

        /**
         * Changes the current active state of this component
         */
        void SetActive(bool state);

        /**
         * @return true is the component is active
         */
        inline bool IsActive() const { return IsInitialized() && active; }
    };
}
