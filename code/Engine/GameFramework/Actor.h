// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include "Core.h"
#include "Component.h"


namespace Engine {
    class World;

    class Actor {
    public:

        Actor()
                : position{Vector2D::Zero}, scale{Vector2D::One}, collisionEnabled{false} {}

        virtual ~Actor() = default;

    private:

        weak_ptr<World> world;

        Vector2D position;
        Vector2D scale;

        vector<unique_ptr<Component>> components;

    protected:

        bool collisionEnabled;


    public:

        virtual bool Initialize(weak_ptr<World> inWorld) {
            if (!inWorld.lock())
                return false;

            world = inWorld;
            return true;
        }

        virtual void Start();

        virtual void Update(float deltaTime);

        virtual void Render(Canvas &canvas);


        /** BEGIN COLLISION */

        /**
         * Check if this Actor collides with another
         * @param other the other actor
         * @return true if there is a collision
         */
        virtual bool CollidesWith(const Actor &other);

        /**
         * Check if this Actor collides with a point in space
         * @param point in space
         * @return true if there is a collision
         */
        virtual bool CollidesWith(const Vector2D &point);

        void EnableCollision() { collisionEnabled = true; }

        void DisableCollision() { collisionEnabled = false; }

        inline bool IsCollisionEnabled() const { return collisionEnabled; }

        /** END COLLISION */


    protected:

        /** BEGIN COMPONENTS */

        template<class T, class... Args>
        T *CreateComponent(Args &&... args) {
            static_assert(is_base_of<Component, T>::value, "Template must inherit from Component");

            unique_ptr<T> newComp{new T(args...)};
            T *rawPtr = newComp.get();

            newComp->Initialize(this);
            components.push_back(move(newComp));
            return rawPtr;
        }

        void DestroyComponent(const Component &comp);

    public:

        template<class C>
        C *GetComponent() {
            for (const unique_ptr<Component> &component : components) {
                C *result = dynamic_cast<C>(*component);
                if (result) {
                    return result;
                }
            }
            return nullptr;
        }

        template<class C>
        vector<C> GetComponents() {
            vector<C> foundComponents;
            for (const unique_ptr<Component> &component : components) {
                C *result = dynamic_cast<C>(*component);
                if (result) {
                    foundComponents.push_back(result);
                }
            }

            return foundComponents;
        }

        /** END COMPONENTS */


        inline Vector2D GetPosition() const {
            return position;
        }

        inline void SetPosition(const Vector2D newPosition) {
            position = newPosition;
        }

        inline Vector2D GetScale() const {
            return scale;
        }

        inline void SetScale(const Vector2D &newScale) {
            scale = newScale;
        }

        virtual World* GetWorld() { return world.lock().get(); }
    };
}
