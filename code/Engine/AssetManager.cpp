// Copyright 2017/2018 - Miguel Fernández Arce

#include <basics/Director>

#include "AssetPtr.h"

#include "AssetManager.h"


namespace Engine {

    AssetManager AssetManager::instance = {};

    void AssetManager::Internal_StaticLoad(const AssetInfo& asset)
    {
        assetInfos.push_back(asset);
    }

    void AssetManager::Internal_StaticLoad(const vector<AssetInfo>& assets)
    {
        assetInfos.insert(assetInfos.end(), assets.begin(), assets.end());
    }
}
