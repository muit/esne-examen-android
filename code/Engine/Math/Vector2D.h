// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include <ostream>
#include <string>
#include <basics/Point>
#include <basics/Size>
using namespace std;


constexpr float DegToRad(const float deg) {
	return (deg * M_PI) / 180.0f;
}


// Añadir los operadores ==, !=, <, <=, >, >=, +, +=, -, -=
// Añadir también operador que permite sumar a izquierda y derecha con un valor
// entero.
// Añadir operador <<

class Vector2D {
public:
	float x, y;

	Vector2D() : Vector2D{0, 0} {}

	Vector2D(const float x, const float y) : x{ x }, y{ y } {}

	Vector2D(const Vector2D&) = default;
	Vector2D(Vector2D&&) = default;

	Vector2D& operator=(const Vector2D&) = default;
	Vector2D& operator=(Vector2D&&) = default;

	~Vector2D() = default;


	void Set(const float inX = 0, const float inY = 0) noexcept {
		x = inX;
		y = inY;
	}

    basics::Point2f ToPoint() const { return { x, y }; }
    basics::Size2f  ToSize()  const { return { x, y }; }

	// Comparison Overloads

    bool operator== (const Vector2D& other) const {
		return x == other.x && y == other.y;
	}

	bool operator!= (const Vector2D& other) const {
		return !(*this == other);
	}

	bool operator< (const Vector2D& other) const {
		// Debería comprobar por magnitud?
		return x < other.x && y < other.y;
	}

	bool operator<= (const Vector2D& other) const {
		return *this < other || *this == other;
	}

	bool operator> (const Vector2D& other) const {
		// Debería comprobar por magnitud?
		return x > other.x && y > other.y;
	}

	bool operator>= (const Vector2D& other) const {
		return *this > other || *this == other;
	}


	// Operation Overloads
	Vector2D operator-() const {
		return { -x, -y };
	}

	Vector2D& operator+=(const Vector2D& rhs) {
		x += rhs.x;
		y += rhs.y;
		return *this;
	}

	Vector2D& operator-=(const Vector2D& rhs) {
		operator+=(-rhs);
		return *this;
	}

	inline Vector2D& operator++() {
		operator+=(Vector2D::One);
		return *this;
	}

	inline Vector2D& operator--() {
		operator-=(Vector2D::One);
		return *this;
	}

	Vector2D& operator*=(const Vector2D& rhs) {
		x *= rhs.x;
		y *= rhs.y;
		return *this;
	}

	Vector2D& operator/=(const Vector2D& rhs) {
		x /= rhs.x;
		y /= rhs.y;
		return *this;
	}

	static const Vector2D Zero;
	static const Vector2D One;

	static Vector2D FromAngle(float angle, float intensity = 1);
};

Vector2D operator+(Vector2D lhs, const Vector2D rhs);
Vector2D operator-(Vector2D lhs, const Vector2D rhs);
Vector2D operator*(Vector2D lhs, const Vector2D rhs);
Vector2D operator*(Vector2D lhs, const float rhs);
Vector2D operator/(Vector2D lhs, const Vector2D rhs);
ostream & operator<<(ostream &os, const Vector2D& p);
