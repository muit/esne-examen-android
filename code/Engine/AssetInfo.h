// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include "Core.h"
#include <basics/Id>


namespace Engine
{
    class AssetInfo
    {
    public:
        AssetInfo() : id(ID_NONE) {}
        AssetInfo(Id inId) : id(inId), path("") {}
        AssetInfo(Id inId, string inPath)
            : id(inId)
            , path(move(inPath))
        {}

    protected:

        Id id;
        string path;

    public:

        /**
         * @returns true if this can never be pointed towards an asset
         */
        const bool IsNull() const {
            return id == ID_NONE;
        }

        inline const Id    GetId()   const { return id;   }
        inline const string& GetPath() const { return path; }
    };
}
