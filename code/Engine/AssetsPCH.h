// Copyright 2017/2018 - Miguel Fernández Arce

#pragma once

#include "AssetManager.h"

#include "AssetPtr.h"
#include "AssetBundle.h"

#include "Assets/AssetData.h"
#include "Assets/Texture.h"
